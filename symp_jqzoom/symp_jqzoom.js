(function ($) {

  /**
   * @file Javascript behaviors for jqzoom module
   */


  Drupal.symp_jqzoom = Drupal.symp_jqzoom || {};

  Drupal.behaviors.symp_jqzoom = {
    attach: function (context, settings) {
      $('a.jqzoom-image-link', context).jqzoom(settings.symp_jqzoom);
    }
  };

})(jQuery);
